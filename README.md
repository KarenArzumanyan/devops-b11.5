# Devops B11.5

DevOps part B11.5 (Pipeline, Mysql, Select from rfam)

# Задание:

Настроить два Pipeline вывода результатов SQL запросов, запускаемых при изменении SQL-скриптов, хранящихся в репозитории.

Для двух запросов:

1) Простейший select, который будет просто выводить первую строку таблицы: 

**query1.sql**
```sql
select fr.* from full_region fr limit 1;
```

2) Выборка всех РНК крыс, хранящиеся в этой базе данных:

**query2.sql**
```sql
SELECT fr.rfam_acc, fr.rfamseq_acc, fr.seq_start, fr.seq_end, f.type
FROM full_region fr, rfamseq rf, taxonomy tx, family f
WHERE
rf.ncbi_id = tx.ncbi_id
AND f.rfam_acc = fr.rfam_acc
AND fr.rfamseq_acc = rf.rfamseq_acc
AND tx.tax_string LIKE '%Mammalia%'
AND f.type LIKE '%snoRNA%'
AND is_significant = 1 -- exclude low-scoring matches from the same clan
```

# Координаты этой базы:

```plaintext
Parameter	Value
host		mysql-rfam-public.ebi.ac.uk
user		rfamro
password	none
port		4497
database	Rfam
```

Для соединения с БД можно использовать:
```console
mysql --user rfamro --host mysql-rfam-public.ebi.ac.uk --port 4497 --database Rfam
```

Для выполнения запроса и вывода результата в файл можно выполнить такую команду:
```console
mysql --user rfamro --host mysql-rfam-public.ebi.ac.uk --port 4497 --database Rfam < query1.sql >> /tmp/query2.result.txt
```
или
```console
mysql --user rfamro --host mysql-rfam-public.ebi.ac.uk --port 4497 --database Rfam < query2.sql >> /tmp/query2.result.txt
```